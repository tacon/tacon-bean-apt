package dev.tacon.bean.apt;

import java.util.function.Consumer;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;

public final class Utils {

	public static String getFieldName(final String methodName) {
		if (methodName.startsWith("get") || methodName.startsWith("set")) {
			return methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
		}
		if (methodName.startsWith("is")) {
			return methodName.substring(2, 3).toLowerCase() + methodName.substring(3);
		}
		return "";
	}

	public static String removeType(final String typeName) {
		final int indexOfLt = typeName.indexOf('<');
		if (indexOfLt > 0) {
			// Barbatrucco: rimuove la tipizzazione dal nome della classe "List<List<String>>[]" deve essere solo "List[]"
			final int lastIndexOfGt = typeName.lastIndexOf('>') + 1;
			final String tmp = typeName.substring(0, indexOfLt) + (lastIndexOfGt == typeName.length() ? "" : typeName.substring(lastIndexOfGt));
			return "(Class<" + typeName + ">) (Class<?>) " + tmp;
		}
		return typeName;
	}

	public static String getTypeName(final TypeMirror type, final Consumer<String> logger) {
		final TypeKind kind = type.getKind();
		String typeName = type.toString();
		if (kind.isPrimitive()) {
			switch (kind) {
				case CHAR:
					typeName = "java.lang.Character";
					break;
				case INT:
					typeName = "java.lang.Integer";
					break;
				// $CASES-OMITTED$
				default:
					final String s = type.toString();
					typeName = "java.lang." + s.substring(0, 1).toUpperCase() + s.substring(1);
					break;
			}
		} else {
			switch (kind) {
				case TYPEVAR:
					typeName = ((TypeVariable) type).getUpperBound().toString();
					break;
				// $CASES-OMITTED$
				default:
					typeName = type.toString();
					break;
			}
		}
		// con le classi java 11 vengono aggiunte anche le annotazioni, ad esempio:
		// (@javax.validation.constraints.Size(max=15, message="Sono permesse al massimo 15 righe di conti sciroppo"),@javax.validation.Valid :: com.azserve.azframework.dto.DtoReferenceWithInteger<? extends com.azserve.registrovitivinicolo.interfaces.data.AbstractOperazioneDTO<?>>)
		// questo comportamento avviene solo lanciandolo con maven e non con eclipse
		// per cui prendo solo la sottostringa dopo l'ultimo spazio
		if (typeName.startsWith("(")) {
			typeName = typeName.substring(1);
			if (typeName.endsWith(")")) {
				typeName = typeName.substring(0, typeName.length() - 1);
			}
			final int doubleColonIndex = typeName.lastIndexOf(" :: ");
			if (doubleColonIndex > 0) {
				typeName = typeName.substring(doubleColonIndex + 4);
			}
		}
		// rimuovo le eventuali annotazioni
		return cleanTypeNameAnnotations(type, typeName, logger);
	}

	private static String cleanTypeNameAnnotations(final TypeMirror type, final String typeName1, final Consumer<String> logger) {
		// logger.accept(">>> " + typeName1);
		String typeName = typeName1;
		if (type instanceof final DeclaredType declared) {
			for (final TypeMirror typeArg : declared.getTypeArguments()) {
				typeName = typeName.replace(typeArg.toString(), cleanTypeNameAnnotations(typeArg, typeArg.toString(), logger));
				// logger.accept("$$$ " + typeName);
			}

			String comma = "";
			for (final AnnotationMirror annotationMirror : declared.getAnnotationMirrors()) {
				// logger.accept(annotationMirror.toString());
				typeName = typeName.replace(comma + annotationMirror.toString(), "");
				// logger.accept("@@@ " + typeName);
				comma = ",";
			}
		} else if (type instanceof final ArrayType array) {
			// logger.accept("ARRAY " + array.getComponentType());
			return cleanTypeNameAnnotations(array.getComponentType(), array.getComponentType().toString(), logger) + "[]";
		} else if (type instanceof final PrimitiveType primitive) {
			String comma = "";
			for (final AnnotationMirror annotationMirror : primitive.getAnnotationMirrors()) {
				// logger.accept(annotationMirror.toString());
				typeName = typeName.replace(comma + annotationMirror.toString(), "");
				// logger.accept("@@@ " + typeName);
				comma = ",";
			}
		} else {
			// logger.accept("******************************** " + typeName1 + " IS " + type.getClass());
		}
		return typeName;

	}

	private Utils() {}
}
