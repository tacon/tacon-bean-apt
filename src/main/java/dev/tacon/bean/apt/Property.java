package dev.tacon.bean.apt;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.RecordComponentElement;
import javax.lang.model.type.TypeMirror;

public class Property {

	public TypeMirror type;
	public Element field;
	public ExecutableElement getterMethod;
	public ExecutableElement setterMethod;

	private boolean complete;

	public static Property ofRecordComponent(final RecordComponentElement recordComponent) {
		return new Property(recordComponent, true);
	}

	public static Property empty() {
		return new Property();
	}

	private Property() {}

	private Property(final Element field, final boolean complete) {
		this.field = field;
		this.type = field.asType();
		this.complete = complete;
	}

	public boolean isComplete() {
		return this.complete
				|| this.type != null && this.field != null && this.getterMethod != null;
	}

	@Override
	public String toString() {
		return this.type + " " + this.field + " " + this.getterMethod + " " + this.setterMethod;
	}
}
