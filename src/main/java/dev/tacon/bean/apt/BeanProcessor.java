package dev.tacon.bean.apt;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.RecordComponentElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import dev.tacon.bean.annotation.Bean;
import freemarker.template.Configuration;
import freemarker.template.Template;

@SupportedAnnotationTypes(value = { "dev.tacon.bean.annotation.Bean" })
@SupportedSourceVersion(SourceVersion.RELEASE_17)
@SuppressWarnings("hiding")
public class BeanProcessor extends AbstractProcessor {

	private String classSuffix = "_";
	private String additionalPackageName = "";
	private Template template = null;
	private Template innerClassTemplate = null;

	private Path path;

	@Override
	public synchronized void init(final ProcessingEnvironment processingEnv) {
		super.init(processingEnv);

		final Messager messager = processingEnv.getMessager();
		final Map<String, String> options = processingEnv.getOptions();

		messager.printMessage(Kind.NOTE, "Starting BeanProcessor");

		this.classSuffix = options.getOrDefault("classsuffix", "_");
		this.additionalPackageName = options.getOrDefault("additionalpackagename", "");

		final Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
		cfg.setClassForTemplateLoading(this.getClass(), "/");
		try {
			this.template = cfg.getTemplate("bean.ftl");
			this.innerClassTemplate = cfg.getTemplate("inner-bean.ftl");
		} catch (final IOException e) {
			messager.printMessage(Kind.ERROR, e.getMessage());
		}
	}

	@Override
	public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv) {
		final Set<TypeElement> typeElements = ElementFilter.typesIn(roundEnv.getElementsAnnotatedWith(Bean.class));
		final Set<TypeElement> neededTypeElements = new HashSet<>();
		for (final TypeElement typeElement : typeElements) {
			addEnclosingTypes(neededTypeElements, typeElement);
		}
		this.processingEnv.getMessager().printMessage(Kind.NOTE, "found " + neededTypeElements.size() + " types");
		neededTypeElements.forEach(this::writeSourceFile);
		return false;
	}

	private static void addEnclosingTypes(final Set<TypeElement> typeElements, final TypeElement typeElement) {
		if (typeElement.getNestingKind().isNested()) {
			final Element enclosingElement = typeElement.getEnclosingElement();
			final ElementKind kind = enclosingElement.getKind();
			if (kind.isClass() || kind.isInterface()) {
				addEnclosingTypes(typeElements, (TypeElement) enclosingElement);
				return;
			}
		}
		typeElements.add(typeElement);
	}

	public void writeSourceFile(final TypeElement typeElement) {
		try {
			final String typeElementContent = this.process(typeElement, this.template, typeElement.getNestingKind().isNested());
			if (!typeElementContent.isEmpty()) {

				final String className = typeElement.getSimpleName().toString();
				final String classPackageName = this.processingEnv.getElementUtils().getPackageOf(typeElement).getQualifiedName().toString();
				final String destinationPackage = classPackageName + this.additionalPackageName;

				final JavaFileObject jfo = this.processingEnv.getFiler().createSourceFile(destinationPackage + "." + className + this.classSuffix);

				this.processingEnv.getMessager().printMessage(Kind.NOTE, "creating source file: " + jfo.toUri());

				try (final Writer writer = jfo.openWriter()) {
					writer.write(typeElementContent);
				}
			}
		} catch (final Exception ex) {
			final String msg = exstr(ex);
			this.processingEnv.getMessager().printMessage(Kind.WARNING, msg, typeElement);
		}
	}

	private static String exstr(final Exception ex) {
		String msg = null;
		final StringWriter stringWriter = new StringWriter();
		try (final PrintWriter printWriter = new PrintWriter(stringWriter)) {
			ex.printStackTrace(printWriter);
			msg = stringWriter.toString();
		}
		return msg;
	}

	private String process(final TypeElement typeElement, final Template template, final boolean topElementIsNested) throws Exception {
		final List<String> innerClasses = new ArrayList<>();
		for (final TypeElement innerTypeElement : ElementFilter.typesIn(typeElement.getEnclosedElements())) {
			if (innerTypeElement.getAnnotation(Bean.class) != null) {
				innerClasses.add(this.process(innerTypeElement, this.innerClassTemplate, topElementIsNested));
			}
		}
		return this.process(typeElement, template, innerClasses);
	}

	private String process(final TypeElement typeElement, final Template template, final List<String> innerClassesContent) throws Exception {

		final String className = typeElement.getQualifiedName().toString();
		this.processingEnv.getMessager().printMessage(Kind.NOTE, "processing class " + className);

		final Map<String, Property> map = new HashMap<>();
		final List<ExecutableElement> methods = ElementFilter.methodsIn(typeElement.getEnclosedElements());

		final Types typeUtils = this.processingEnv.getTypeUtils();
		final boolean isRecord = ElementKind.RECORD == typeElement.getKind();

		if (isRecord) {
			final List<RecordComponentElement> fields = ElementFilter.recordComponentsIn(typeElement.getEnclosedElements());
			for (final RecordComponentElement field : fields) {
				final String fieldName = field.getSimpleName().toString();
				map.put(fieldName, Property.ofRecordComponent(field));
			}
		} else {

			for (final ExecutableElement method : methods) {
				final String methodName = method.getSimpleName().toString();
				// metodi pubblici, non statici, senza parametri che iniziano per
				// "is" o "get"
				if (method.getModifiers().contains(Modifier.PUBLIC) && !method.getModifiers().contains(Modifier.STATIC)
						&& method.getParameters().isEmpty() && methodName.startsWith("get")
						|| methodName.startsWith("is")) {
					final String fieldName = Utils.getFieldName(methodName);
					final Property p = Property.empty();
					p.getterMethod = method;
					p.type = method.getReturnType();
					map.put(fieldName, p);
				}
			}

			for (final ExecutableElement method : methods) {
				final String methodName = method.getSimpleName().toString();
				// metodi pubblici, non statici, senza parametri che iniziano per
				// "is" o "get"
				if (method.getModifiers().contains(Modifier.PUBLIC) && !method.getModifiers().contains(Modifier.STATIC)
						&& method.getParameters().size() == 1 && methodName.startsWith("set")) {
					final String fieldName = Utils.getFieldName(methodName);
					final Property property = map.get(fieldName);
					if (property != null) {
						final VariableElement parameter = method.getParameters().iterator().next();
						final TypeMirror type = parameter.asType();
						if (typeUtils.isSameType(type, property.type)) {
							property.setterMethod = method;
						} else {
							map.remove(fieldName);
						}
					}
				}
			}
			final List<VariableElement> fields = ElementFilter.fieldsIn(typeElement.getEnclosedElements());
			for (final VariableElement field : fields) {
				if (!field.getModifiers().contains(Modifier.STATIC)) {
					final String fieldName = field.getSimpleName().toString();
					final Property property = map.get(fieldName);
					if (property != null) {
						final TypeMirror type = field.asType();
						if (typeUtils.isSameType(type, property.type)) {
							property.field = field;
						} else {
							map.remove(fieldName);
						}
					}
				}
			}
		}

		final Iterator<Entry<String, Property>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			final Property property = it.next().getValue();
			if (!property.isComplete()) {
				it.remove();
			}
		}

		this.processingEnv.getMessager().printMessage(Kind.NOTE, "processing " + map.size() + " fields for class " + className);

		return this.generateClass(typeElement, map, template, innerClassesContent);
	}

	private String generateClass(final TypeElement typeElement, final Map<String, Property> map, final Template template, final List<String> innerClassesContent) throws Exception {
		final String fullClassName = typeElement.getQualifiedName().toString();
		this.processingEnv.getMessager().printMessage(Kind.NOTE, "writing metadata class for " + fullClassName);

		final String className = typeElement.getSimpleName().toString();
		final String classPackageName = this.processingEnv.getElementUtils().getPackageOf(typeElement).getQualifiedName().toString();
		final String destinationPackage = classPackageName + this.additionalPackageName;

		final Map<String, Object> vc = new HashMap<>();

		// this.log(className);

		vc.put("classSuffix", this.classSuffix);
		vc.put("fullClassName", fullClassName);
		vc.put("className", className);
		vc.put("classPackageName", classPackageName);
		vc.put("packageName", destinationPackage);
		vc.put("fields", map.entrySet().stream().map(this::createBeanField).toList());
		vc.put("innerClasses", innerClassesContent);

		try (StringWriter sw = new StringWriter()) {
			template.process(vc, sw);
			return sw.toString();
		}
	}

	private BeanField createBeanField(final Entry<String, Property> e) {
		final Element field = e.getValue().field;
		final String fieldName = field.getSimpleName().toString();
		final TypeMirror type = field.asType();
		final String typeName = Utils.getTypeName(type, this::log);
		String classTypeName = typeName;

		final int indexOfLt = typeName.indexOf('<');
		if (indexOfLt > 0) {
			// this.log(type.toString() + " | " + typeName);

			// Barbatrucco: rimuove la tipizzazione dal nome della classe "List<List<String>>[]" deve essere solo "List[]"
			final int lastIndexOfGt = typeName.lastIndexOf('>') + 1;
			final String tmp = typeName.substring(0, indexOfLt) + (lastIndexOfGt == typeName.length() ? "" : typeName.substring(lastIndexOfGt));
			classTypeName = "(Class<" + typeName + ">) (Class<?>) " + tmp;
		} else if (type.getKind().isPrimitive()) {
			// devo passare nel costruttore l'oggetto int.class e non Integer.class, mentre per la tipizzazione devo usare <Integer>
			classTypeName = type.toString();
		}

		return new BeanField(fieldName, typeName, classTypeName);
	}

	protected void log(final String s) {
		if (this.path == null) {
			try {
				this.path = Files.createTempFile("bean-processor", ".log");
			} catch (final IOException e1) {
				throw new RuntimeException(e1);
			}
		}
		try {
			Files.write(this.path, (s + "\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static class BeanField {

		private final String name;
		private final String type;
		private final String classType;

		public BeanField(final String name, final String type, final String classType) {
			super();
			this.name = name;
			this.type = type;
			this.classType = classType;
		}

		public String getName() {
			return this.name;
		}

		public String getType() {
			return this.type;
		}

		public String getClassType() {
			return this.classType;
		}
	}
}
