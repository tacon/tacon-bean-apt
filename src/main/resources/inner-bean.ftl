	public static final class ${className}${classSuffix} {

		<#list fields as field>
		/**
		 * Ref to property {@code ${field.name}}.
		 */
		public static final RootBeanProperty<${fullClassName}, ${field.type}> ${field.name} = new RootBeanProperty<>(${fullClassName}.class, "${field.name}", ${field.classType}.class);

		</#list>


		/**
		 * List of all properties
		 */
		public static List<RootBeanProperty<${fullClassName}, ?>> properties() {
			return List.of(
				<#list fields as field>
				${field.name}<#sep>,</#sep>
				</#list>
			);
		}

		private ${className}${classSuffix}() {}

		<#list innerClasses as innerClass>
			${innerClass}
		</#list>
	}