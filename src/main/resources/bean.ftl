package ${packageName};

import ${fullClassName};
import java.util.List;
import dev.tacon.bean.BeanProperty.RootBeanProperty;

/**
 * Metadata for class {@linkplain ${classPackageName}.${className}}.
 */
@SuppressWarnings({"all"})
public final class ${className}${classSuffix} {

	<#list fields as field>
	/**
	 * Ref to property {@code ${field.name}}.
	 */
	public static final RootBeanProperty<${className}, ${field.type}> ${field.name} = new RootBeanProperty<>(${className}.class, "${field.name}", ${field.classType}.class);

	</#list>

	/**
	 * List of all properties
	 */
	public static List<RootBeanProperty<${className}, ?>> properties() {
		return List.of(
			<#list fields as field>
			${field.name}<#sep>,</#sep>
			</#list>
		);
	}

	private ${className}${classSuffix}() {}

	<#list innerClasses as innerClass>
${innerClass}
	</#list>
}